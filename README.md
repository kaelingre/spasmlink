# spaSMLink: Mathematica LibraryLink to spaSM v1.3

This is an efficient implementation of a LibraryLink to SpaSM (Sparse direct Solver Modulo p) [https://github.com/cbouilla/spasm].
A modified version of SpaSM itself is included in this repository.

## 1 Installation as a pre-compiled Mathematica paclet

Simply evaluate the following command within Mathematica
```
PacletInstall[CloudObject["https://www.wolframcloud.com/obj/kaelingre/spaSMLink-1.3.paclet"]];
```

## 2 Installation via manual compilation as Mathematica package

### 2.1 Prerequisits

- Linux only at the moment. OSX should be possible with some tweaks to cmake build files.
- SpaSM's requirements: Givaro & FFLAS-FFPACK
- Mathematica 13+
  - If Mathematica is installed in a non-default location, you might have to add its path to line 489 in the file CMake/Mathematica/FindMathematica.

### 2.2 Compilation and installation

Type
```
make
```
in the top source directory and pray it compiles.

Now the spaSMLink folder needs to be either copied or symlinked to into `$UserBaseDir<>"/Applications"`, e.g. on linux something like
```bash
ln -s /path/to/spaSMLink/spaSMLink/ ~/.Mathematica/Applications/spaSMLink
```
should work

## 3 Usage

The package can be loaded into Mathematica with
```
Needs["spaSMLink`"];
```
Check the documentation for the most useful functions to get an idea what the package can do for you:
```
?spasmRRef
?spasmReduce
?spasmSolve
```

## 4 code used from other projects:
- SpaSM [https://github.com/cbouilla/spasm], for the license see `spasm/COPYING`. Please consider citing
```
@manual{spasm,
title = {{SpaSM}: a Sparse direct Solver Modulo $p$},
author = {Charles Bouillaguet},
edition = {v1.3},
year = {2023},
note = {\url{http://github.com/cbouilla/spasm}}
}
```
if you have used this code in your research project.
- FindMathematica [https://github.com/sakra/FindMathematica], for the license see `spasm/CMake/Mathematica/LICENSE`.

## 5 Feedback, bug reports, and feature requests
I'm happy to hear from you either via email to kaelingre'at'gmail.com or using the issue tracker here on gitlab.