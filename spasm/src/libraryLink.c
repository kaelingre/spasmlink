#include "libraryLink.h"

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

EXTERN_C DLLEXPORT int LL_rref(WolframLibraryData libData_, mint argc_, MArgument* args_,
							   MArgument res_) {
	if (!(argc_ == 1 || argc_ == 2)) {
		return LIBRARY_FUNCTION_ERROR;
	}
	
	//get input SparseArray
	MSparseArray arg = MArgument_getMSparseArray(args_[0]);
	//get the prime number
	i64 prime = 4294967291;//=largest possible prime < \sqrt{2^64}
	if (argc_ == 2) {
		prime = MArgument_getInteger(args_[1]);
	}

	struct spasm_csr* A = fromMathematica(&arg, libData_, prime);
	
	//do the reduction
	struct echelonize_opts opts;
	spasm_echelonize_init_opts(&opts);
	struct spasm_lu* LU = spasm_echelonize(A, &opts);

	int *Rqinv = spasm_malloc(A->m * sizeof(int));
	struct spasm_csr* R = spasm_rref(LU, Rqinv);

	printf("rref done\n");

	//return to Mathematica
	MSparseArray ret = toMathematica(R, Rqinv, libData_);

	//clean up
	spasm_csr_free(A);
	free(Rqinv);

	//return to mathematica
	MArgument_setMSparseArray(res_, ret);

	return LIBRARY_NO_ERROR;
}

EXTERN_C DLLEXPORT int WolframLibrary_initialize(WolframLibraryData libData_) {
	UNUSED(libData_);
	return LIBRARY_NO_ERROR;
}

EXTERN_C DLLEXPORT void WolframLibrary_uninitialize(WolframLibraryData libData_) {
	UNUSED(libData_);
}

struct spasm_csr* fromMathematica(MSparseArray* arg_, WolframLibraryData libData_, const int64_t prime_) {
	//get pointer to sparse functions
	WolframSparseLibrary_Functions sparseFuns = libData_->sparseLibraryFunctions;

	//get the dimensions of the sparse array
	mint const* dims = (*(sparseFuns->MSparseArray_getDimensions))(*arg_);
	
	//get the values array of the sparse array
	MTensor* values = (*(sparseFuns->MSparseArray_getExplicitValues))(*arg_);
	//number of (non-zero) entries
	mint n = libData_->MTensor_getFlattenedLength(*values);
	mint* valuesArray = libData_->MTensor_getIntegerData(*values);
	
	//get the column indices of the sparse array
	mint* columnIndices = libData_->MTensor_getIntegerData(*(*(sparseFuns->MSparseArray_getColumnIndices))(*arg_));

	//get the row pointers of the sparse array
	mint* rowPointers = libData_->MTensor_getIntegerData(*(*(sparseFuns->MSparseArray_getRowPointers))(*arg_));

	//prepare the spasm csr
	struct spasm_csr *A = spasm_csr_alloc(dims[0], dims[1], n, prime_, true);

	//copy the three arrays into spasm's csr
	spasm_ZZp* Ax = A->x;
	int* Aj = A->j;
	for (i64 i = 0; i < n; ++i) {
		Ax[i] = valuesArray[i];
		Aj[i] = columnIndices[i] - 1;
	}
	i64* Ap = A->p;
	for (int i = 0; i < dims[0] + 1; ++i) {
		Ap[i] = rowPointers[i];
	}

	return A;
}

MSparseArray toMathematica(const struct spasm_csr* R_, const int* Rqinv_, WolframLibraryData libData_) {
	//get pointer to sparse functions
	WolframSparseLibrary_Functions sparseFuns = libData_->sparseLibraryFunctions;
	
	MTensor posRet, valuesRet;
	i64* Up = R_->p;
	int n = R_->n;
	int m = R_->m;

	//number of entries
	mint counter = R_->p[n] + /*for qinv*/n;
	
	const mint dimsPos[] = {counter,2};
	const mint dimsValues[] = {counter};
	libData_->MTensor_new(MType_Integer, 2, dimsPos, &posRet);
	libData_->MTensor_new(MType_Integer, 1, dimsValues, &valuesRet);
	mint posCounter[] = {1,1}, valuesCounter = 1;
	int* Uj = R_->j;
	spasm_ZZp* Ux = R_->x;
	//read out the positions and values
	for (int i = 0; i < n; ++i) {
		for (i64 px = Up[i]; px < Up[i + 1]; ++px) {
			i64 x = (Ux != NULL) ? Ux[px] : 1;
			libData_->MTensor_setInteger(posRet, posCounter, i + 1);
			++posCounter[1];
			libData_->MTensor_setInteger(posRet, posCounter, Uj[px] + 1);
			++posCounter[0];
			posCounter[1] = 1;
			libData_->MTensor_setInteger(valuesRet, &valuesCounter, x);
			++valuesCounter;
		}
	}

	//set the last row to Rqinv
	for (int j = 0; j < m; ++j) {
		const int* val = &Rqinv_[j];
		if (*val > - 1) {
			libData_->MTensor_setInteger(posRet, posCounter, n + 1);
			++posCounter[1];
			libData_->MTensor_setInteger(posRet, posCounter, j + 1);
			++posCounter[0];
			posCounter[1] = 1;
			libData_->MTensor_setInteger(valuesRet, &valuesCounter, *val + 1);
			++valuesCounter;
		}
	}

	//construct return SparseArray
	MSparseArray ret = 0;
	mint dimsRet[] = {2};
	MTensor dimsRetTens;
	libData_->MTensor_new(MType_Integer, 1, dimsRet, &dimsRetTens);
	dimsRet[0] = 1;
	libData_->MTensor_setInteger(dimsRetTens, dimsRet, R_->n + 1);
	dimsRet[0] = 2;
	libData_->MTensor_setInteger(dimsRetTens, dimsRet, R_->m);
	(*(sparseFuns->MSparseArray_fromExplicitPositions))(posRet, valuesRet, dimsRetTens, 0, &ret);

	//free mathematica stuff
	libData_->MTensor_free(posRet);
	libData_->MTensor_free(valuesRet);
	libData_->MTensor_free(dimsRetTens);

	return ret;
}

