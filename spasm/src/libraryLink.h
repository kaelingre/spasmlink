#ifndef LIBRARYLINK_H
#define LIBRARYLINK_H

#define UNUSED(x) (void)(x)

#include "WolframLibrary.h"
#include "WolframSparseLibrary.h"
#include "spasm.h"

EXTERN_C DLLEXPORT int WolframLibrary_getVersion() {
	return WolframLibraryVersion;
}

/**
 * The actual libary link function that is being loaded into Mathematica.
 * It performs imports a SparseArray A from Mathematica and exports its rref form R back
 * into Mathematica. Note that the rref is done WITH up to a column permutation Q. Meaning R.Q = rref(A.Q).
 * We return R and also the columns that would need to be moved in front to make it rref.
 * It is encoded in the last row of the returned SparseArray in the same format as qinv in
 * the spasm_lu struct, essentially giving the locations of the pivots.
 */
EXTERN_C DLLEXPORT int LL_rref(WolframLibraryData libData_, mint argc_, MArgument* args_, MArgument res_);

EXTERN_C DLLEXPORT int WolframLibrary_initialize(WolframLibraryData libData_);

EXTERN_C DLLEXPORT void WolframLibrary_uninitialize(WolframLibraryData libData_);

/** Load a MSparseArray into a spasm_csr */
struct spasm_csr* fromMathematica(MSparseArray* arg_, WolframLibraryData libData_, const int64_t prime_);

/** Export the row-reduced matrix R_ and the permutation q_ into a MMA Sparsearray */
MSparseArray toMathematica(const struct spasm_csr* R_, const int* Rqinv_, WolframLibraryData libData_);

#endif /*LIBRARYLINK_H*/
