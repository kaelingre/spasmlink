(* ::Package:: *)

PacletObject[
	<|
		"Name" -> "spaSMLink",
		"Description" -> "A LibraryLink package to SpaSM (Sparse direct Solver Modulo p)",
		"Version" -> "1.3",
		"WolframVersion" -> "13.0+",
		"SystemID" -> {"Linux-x86-64"},
		"Extensions" -> {
			{"Kernel", "Root" -> "Kernel", "Context" -> {"spaSMLink`"}},
			{"LibraryLink"}
		}
	|>
]
