(* ::Package:: *)

Quit[];


(* ::Section:: *)
(*Load*)


Needs["PacletTools`"];


pac=PacletObject[File[NotebookDirectory[]]]


(* ::Section:: *)
(*Documentation*)


(* ::Text:: *)
(*Open: Palettes > Documentation Tools*)


(* ::Section:: *)
(*Build*)


CurrentValue[$FrontEnd, DefaultStyleDefinitions] = "Default.nb";


(*build the paclet*)
build=PacletBuild[NotebookDirectory[]]


path=build["PacletArchive"]


(* ::Section:: *)
(*Install*)


PacletInstall[path,ForceVersionInstall -> True]


(* ::Section:: *)
(*Upload to Wolfram Cloud*)


CloudConnect[];


DeleteFile[CloudObject[StringSplit[path,"/"][[-1]]]];
obj=CopyFile[path,CloudObject[StringSplit[path,"/"][[-1]]]];


Options[obj,Permissions]


SetPermissions[obj,"Public"]


Options[obj,Permissions]
