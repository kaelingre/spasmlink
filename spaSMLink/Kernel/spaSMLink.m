(* ::Package:: *)

BeginPackage["spaSMLink`"]
Unprotect@@Names["spaSMLink`*"];


(* ::Section::Closed:: *)
(*Public interface*)


spaSMLink::libNotFound = 
"C++ library could not be loaded.
 Please install the package in the directory: 
 " <> $UserBaseDirectory <> "/Applications/"
 
 (*Load shared C++ library*)
If[FindLibrary["libspasm"]===$Failed,Message[spaSMLink::libNotFound];Abort[]; LibraryLoad["libspasm"];]

spasmRref::usage = "spasmRref[mat_SparseArray, prime_Integer] performs a row reduction up to column permutations in SpaSM of the (sparse) matrix mat modulo a prime number.
The function returns a pair of the row reduced form (up to column permutations) and a list of the pivot positions, i.e. for which variables this system is optimally solved."
spasmRref::author = "Gregor Kaelin";

reconstruct::usage = "reconstruct[num_Integer, prime_Integer] rational reconstruction of numbers module a prime.";
reconstruct::author = "Gregor Kaelin";

chineseRemainder::usage = "Combines numbers (mod prime) via the Chinese Remainder Theorem. First argument: list of numbers. Second argument: list of primes";
chineseRemainder::author = "Gregor Kaelin";

chineseRemainderMats::usage = "Applies the chineseRemainder function on a collection of sparse matrices";
chineseRemainderMats::author = "Gregor Kaelin";

primeList::usage = "primeList[n_:1000] returns a list of the largest n primes that can be used with SpaSM";
primeList::author = "Gregor Kaelin";

rationalToInt::usage = "rationalToInt[rat_,prime_] Project a rational number onto the finite field modulo prime";
rationalToInt::author = "Alex Edison";

spasmReduce::usage = "spasmReduce[matrix_SparseArray, primes_List] performs a row reduction (up to column permutations) of the given system in SpaSM module multiple primes and tries to reconstruct the exact answer.
The function returns a pair of the row reduced form (up to column permutations) and a list of the pivot positions, i.e. for which variables this system is optimally solved.";
spasmReduce::author = "Gregor Kaelin";

spasmSolve::usage = "spasmSolve[matrix_SparseArray, vars_List, primes_List] tries to solve the given linear system for given variables using SpaSM's row reduction for multiple primes and reconstructs the exact answer."
spasmSolve::author = "Gregor Kaelin";


(* ::Section::Closed:: *)
(*Private implementations*)


Begin["`Private`"]
(* Implementation of the package *)


spasmRref[matrix_SparseArray, prime_Integer : 4294967291] := With[{tmp=LLrref[matrix, prime]},
	With[{pos=First/@tmp[[-1]]["ExplicitPositions"]},
		{tmp[[tmp[[-1]]["ExplicitValues"]]],pos}
	]
]

LLrref::usage = "(C++) direct LibraryFunction in c++"
LLrref::author = "Gregor Kaelin"
LLrref = LibraryFunctionLoad["libspasm", "LL_rref", {{LibraryDataType[SparseArray, Integer, 2], "Constant"}, Integer}, {LibraryDataType[SparseArray, Integer, 2], Automatic}]


reconstruct[0, _] := 0
reconstruct[a_, p_] := reconstruct[a,p] = Block[{rr,ss,tt,qq,i=1},
	rr[0] = a;
	ss[0] = 1;
	tt[0] = 0;
	rr[1] = p;
	ss[1] = 0;
	tt[1] = 1;
	qq[j_] := qq[j] = Floor[rr[j-2]/rr[j-1]];
	rr[j_] := rr[j] = rr[j-2]-qq[j]rr[j-1];
	ss[j_] := ss[j] = ss[j-2]-qq[j]ss[j-1];
	tt[j_] := tt[j] = tt[j-2]-qq[j]tt[j-1];
	While[True,
		If[rr[i]^2 < rr[1] && ss[i]^2 < rr[1], Return[rr[i]/ss[i]];];
		i++;];
	$Failed
]


chineseRemainderMats[{matrices__SparseArray}, primes_List] := With[{nonZeroPos = DeleteDuplicates[Flatten[#["NonzeroPositions"]&/@{matrices}, 1]]},
    SparseArray[
    	Table[
        	pos->ChineseRemainder[#[[Sequence@@pos]]&/@{matrices}, primes]
	    	,{pos, nonZeroPos}]
    	, Dimensions[{matrices}[[1]]]]
]


primeList[n_:1000]:=Prime[#]&/@Range[203280221,203280222-n,-1]


rationalToInt[0,pp_Integer:4294967291]:=0
rationalToInt[aa_Rational,pp_Integer:4294967291]:=Mod[Numerator[aa]*PowerMod[Denominator[aa],-1,pp],pp]
rationalToInt[aa_Integer,pp_Integer:4294967291]:=Mod[aa,pp]


spasmReduce[matrix_SparseArray, primes_List]:=With[{reductions=spasmRref[matrix,#]&/@primes,p=Times@@primes},
	If[!SameQ@@reductions[[All,2]],Return[$Failed]];
	{ap[reconstruct[#,p]&,chineseRemainderMats[reductions[[All,1]],primes],{2}],reductions[[1,2]]}
]


spasmSolve[matrix_SparseArray, vars_List, primes_List]:=With[{tmp=spasmReduce[matrix,primes]},
	Solve[#==0&/@(tmp[[1]] . vars),vars[[tmp[[2]]]]][[1]]
]


(* ::Section:: *)
(*End Matter*)


End[]
(*Allow memoized functions to write to themselves*)
Unprotect[reconstruct]
EndPackage[]
